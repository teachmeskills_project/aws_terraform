resource "aws_key_pair" "master_key" {
  key_name = "master_key"
  public_key = var.PUBLIC_KEY
  tags = {
      "Name" = "master_key"
  }
}

resource "aws_key_pair" "app_key" {
  key_name = "app_key"
  public_key = var.PUBLIC_JENKINS_KEY
  tags = {
      "Name" = "app_key"
  }
}

