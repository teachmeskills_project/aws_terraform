resource "aws_instance" "jenkins" {
    ami = "ami-06ec8443c2a35b0ba"
    instance_type = "t3.small"
    subnet_id = aws_subnet.public_tms_1.id
    associate_public_ip_address = true
    key_name = aws_key_pair.master_key.key_name
    vpc_security_group_ids = [ aws_security_group.sg_jenkins.id ]
    tags = {
        "Name" = "Jenkins"
    }
}

resource "aws_instance" "app_server_1" {
    ami = "ami-06ec8443c2a35b0ba"
    instance_type = "t3.small"
    subnet_id = aws_subnet.public_tms_1.id
    associate_public_ip_address = true
    key_name = aws_key_pair.app_key.key_name
    vpc_security_group_ids = [ aws_security_group.sg_app.id ]
    tags = {
        "Name" = "App_server_1"
    }
}

resource "aws_instance" "app_server_2" {
    ami = "ami-06ec8443c2a35b0ba"
    instance_type = "t3.small"
    subnet_id = aws_subnet.public_tms_1.id
    associate_public_ip_address = true
    key_name = aws_key_pair.app_key.key_name
    vpc_security_group_ids = [ aws_security_group.sg_app.id ]
    tags = {
        "Name" = "App_server_2"
    }
}

