resource "aws_vpc" "tms_vpc" {
    cidr_block = "10.0.0.0/16"
    enable_dns_support = true
    enable_dns_hostnames = true
    tags = {
        Name = "VPC_TMS"
    }  
}

resource "aws_internet_gateway" "gw_tms" {
  vpc_id = aws_vpc.tms_vpc.id
  tags = {
    "Name" = "GW_TMS"
  }
}

resource "aws_subnet" "public_tms_1" {
  vpc_id = aws_vpc.tms_vpc.id
  cidr_block = "10.0.11.0/24"
  availability_zone = "eu-central-1a"
  tags = {
    "Name" = "Public-1-A"
  }
}

resource "aws_subnet" "private_tms_1" {
  vpc_id = aws_vpc.tms_vpc.id
  cidr_block = "10.0.21.0/24"
  availability_zone = "eu-central-1a"
  tags = {
    "Name" = "Private-1-A"
  }
}

// resource "aws_subnet" "dev_database" {
//   vpc_id = aws_vpc.vpc_dev.id
//   cidr_block = "10.0.31.0/24"
//   availability_zone = "eu-central-1a"
//   tags = {
//     "Name" = "Database-1-A"
//   }
// }

resource "aws_route_table" "public_routes" {
  vpc_id = aws_vpc.tms_vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw_tms.id
  }
  tags = {
    "Name" = "PublicRoutes"
  }
}

resource "aws_route_table_association" "public_dev_add" {
  subnet_id = aws_subnet.public_tms_1.id
  route_table_id = aws_route_table.public_routes.id
}
