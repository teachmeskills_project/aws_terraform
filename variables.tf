variable "PUBLIC_KEY" {
    type = string
}

variable "PUBLIC_JENKINS_KEY" {
    type = string
}
