terraform {
    required_providers {
        aws = {
        source = "hashicorp/aws"
        version = "~> 3.0"
        }
    }
    ## после первого запуска расскоментировать

    backend "s3" {
        bucket = "alehbayarevich.art"
        key = "tms-devops5/bayarevichaleh/terraform.tfstate"
        region = "eu-central-1"
        dynamodb_table = "Lock-Bayarevich"
    }
}

provider "aws" {
}
